package it.feio.android.backlightkeys;

import it.feio.backlightkeys.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import it.feio.android.backlightkeys.Constants;

public class MainActivity extends Activity implements OnClickListener, Constants {

	Intent serviceIntent;
	BroadcastReceiver screenReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button startBtn = (Button) findViewById(R.id.startBtn);
		startBtn.setOnClickListener(this);
		Button stopBtn = (Button) findViewById(R.id.stopBtn);
		stopBtn.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.startBtn:
			startService();
			break;

		case R.id.stopBtn:
			stopService();
			break;
		}
	}

	public void startService() {
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        screenReceiver = new ScreenReceiver();
		registerReceiver(screenReceiver, filter);
		
		Toast.makeText(this, "Servizio Avviato", Toast.LENGTH_SHORT).show();
		Log.i(TAG, "Servizio Avviato");
	}

	public void stopService() {
//		stopService(serviceIntent);
		unregisterReceiver(screenReceiver);
		

		Toast.makeText(this, "Servizio Terminato", Toast.LENGTH_SHORT).show();
		Log.i("BackLightKeys", "Servizio Terminato");
	}

}
